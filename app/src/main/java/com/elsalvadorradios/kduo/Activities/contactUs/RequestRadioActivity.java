package com.elsalvadorradios.kduo.Activities.contactUs;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.ColorDrawable;
import android.media.Image;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.util.Log;
import android.util.Patterns;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import com.elsalvadorradios.kduo.MainActivity;
import com.elsalvadorradios.kduo.R;
import com.elsalvadorradios.kduo.helpers.TransistorKeys;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.regex.Pattern;


public class RequestRadioActivity extends AppCompatActivity implements View.OnClickListener, PopupMenu.OnMenuItemClickListener {

    TextView stationNameTV;
    TextView streamLinkTV;

    EditText stationNameET;
    EditText streamLinkET;
    EditText topicET;
    EditText genreET;
    EditText websiteET;
    EditText logoET;

    ImageView showGenreImageView;
    ImageView showTopicImageView;
    ImageView addImageImageView;

    Button submitButton;

    boolean imageSelected = false;


    ProgressDialog progressDialog;

    String userEmailAddress;
    String picturePath;
    String logo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request_radio);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);


        bindActivity();
    }

    private void bindActivity() {
        stationNameTV = (TextView) findViewById(R.id.stationNameTextView);
        stationNameTV.setText((Html.fromHtml(TransistorKeys.stationNameText)));

        streamLinkTV = (TextView) findViewById(R.id.streamlinkTextView);
        streamLinkTV.setText(Html.fromHtml(TransistorKeys.streamUrlText));

        showGenreImageView = (ImageView) findViewById(R.id.showGenreImageView);
        showGenreImageView.setOnClickListener(this);

        showTopicImageView = (ImageView) findViewById(R.id.showTopicImageView);
        showTopicImageView.setOnClickListener(this);

        addImageImageView = (ImageView) findViewById(R.id.addImageImageView);
        addImageImageView.setOnClickListener(this);

        stationNameET = (EditText) findViewById(R.id.stationNameET);
        streamLinkET = (EditText) findViewById(R.id.stationUrlET);
        topicET = (EditText) findViewById(R.id.topicET);
        genreET = (EditText) findViewById(R.id.genreET);
        websiteET = (EditText) findViewById(R.id.websiteET);
        logoET = (EditText) findViewById(R.id.logoET);

        submitButton = (Button) findViewById(R.id.submitButton);
        submitButton.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.submitButton:


                String stationName = stationNameET.getText().toString();
                String streamUrl = streamLinkET.getText().toString();
                String topic = topicET.getText().toString();
                String genre = genreET.getText().toString();
                String website = websiteET.getText().toString();

                if (stationName.length() <= 0) {
                    stationNameET.setError("This Field is cumpulsory.");
                    break;
                } else if (streamUrl.length() <= 0) {
                    streamLinkET.setError("This Field is cumpulsory.");
                    break;

                } else {
                    logo = logoET.getText().toString();

                    if (logo.length() <= 0) {
                        logo = " ";
                    }

                    progressDialog = new ProgressDialog(RequestRadioActivity.this);
                    progressDialog.setMessage("Sending...");
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.show();

                    try {

                        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
                        Account[] accounts = AccountManager.get(RequestRadioActivity.this).getAccounts();
                        for (Account account : accounts) {
                            if (emailPattern.matcher(account.name).matches()) {
                                userEmailAddress = account.name;
                            }
                        }


                        String body = "Station Name : " + stationName + "\n" +
                                "Station Url : " + streamUrl + "\n" +
                                "Station Topic : " + topic + "\n" +
                                "Station Genre : " + genre + "\n" +
                                "Station Website : " + website + "\n" +
                                "Sent by : " + userEmailAddress + "\n";

                        GMailSender sender = new GMailSender("requestedradiokduo@gmail.com", "kduorequest");
                        sender.sendMail("Radio Station Request",
                                body,
                                userEmailAddress,
                                "rasilr10@gmail.com", logo , imageSelected);


                        ViewDialog viewDialog = new ViewDialog();
                        viewDialog.showDialog(RequestRadioActivity.this);

                    } catch (Exception e) {
                        Log.e("SendMail", e.getMessage(), e);
                    }
                }

                break;

            case R.id.showGenreImageView:
                PopupMenu genrePopUp = new PopupMenu(this, v);
                genrePopUp.setOnMenuItemClickListener(this);
                genrePopUp.inflate(R.menu.menu_genre);
                genrePopUp.show();

                break;

            case R.id.showTopicImageView:
                PopupMenu topicPopUp = new PopupMenu(this, v);
                topicPopUp.setOnMenuItemClickListener(this);
                topicPopUp.inflate(R.menu.menu_topic);
                topicPopUp.show();

                break;

            case R.id.addImageImageView:

                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("image/*");
                String[] mimeTypes = {"image/jpeg", "image/png"};
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
                startActivityForResult(intent, TransistorKeys.GALLERY);
                break;
        }

    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != RESULT_OK) {
            return;
        }
        if (requestCode == TransistorKeys.GALLERY) {
            final Bundle extras = data.getExtras();
            if (extras != null) {
                //Get image
//                Bitmap attachedImageBitmap = extras.getParcelable("data");
//                addImageImageView.setImageBitmap(attachedImageBitmap);

                Uri selectedImage = data.getData();

                picturePath = getPath(RequestRadioActivity.this.getApplicationContext(), selectedImage);

                addImageImageView.setImageURI(selectedImage);
                logoET.setText(picturePath);
                logoET.setVisibility(View.GONE);
                imageSelected = true;


            }
        }
    }

    public static String getPath(Context context, Uri uri) {
        String result = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = context.getContentResolver().query(uri, proj, null, null, null);
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                int column_index = cursor.getColumnIndexOrThrow(proj[0]);
                result = cursor.getString(column_index);
            }
            cursor.close();
        }
        if (result == null) {
            result = "Not found";
        }
        return result;
    }

    @Override
    public boolean onMenuItemClick(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.genrePop:
                genreET.setText(item.getTitle());
                return true;
            case R.id.genreClassic:
                genreET.setText(item.getTitle());
                return true;
            case R.id.genreRock:
                genreET.setText(item.getTitle());
                return true;
            case R.id.genreTalk:
                genreET.setText(item.getTitle());
                return true;
            case R.id.genreReligion:
                genreET.setText(item.getTitle());
                return true;
            case R.id.genreNews:
                genreET.setText(item.getTitle());
                return true;
            case R.id.genreSports:
                genreET.setText(item.getTitle());
                return true;
            case R.id.genreOther:
                genreET.setText(item.getTitle());
                return true;

            case R.id.topicPop:
                topicET.setText(item.getTitle());
                return true;
            case R.id.topicClassic:
                topicET.setText(item.getTitle());
                return true;
            case R.id.topicRock:
                topicET.setText(item.getTitle());
                return true;
            case R.id.topicTalk:
                topicET.setText(item.getTitle());
                return true;
            case R.id.topicReligion:
                topicET.setText(item.getTitle());
                return true;
            case R.id.topicNews:
                topicET.setText(item.getTitle());
                return true;
            case R.id.topicSports:
                topicET.setText(item.getTitle());
                return true;
            case R.id.topicOther:
                topicET.setText(item.getTitle());
                return true;
            default:
                return false;
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public class ViewDialog {

        public void showDialog(Activity activity) {
            if (progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog.cancel();
            }
            Dialog dialog = new Dialog(activity);
            dialog.setCanceledOnTouchOutside(true);

            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setCancelable(true);
            dialog.setContentView(R.layout.dialog_submitted);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);

            WindowManager.LayoutParams lp = dialog.getWindow().getAttributes();
            lp.dimAmount = 0.7f;
            dialog.getWindow().setAttributes(lp);
            dialog.getWindow().addFlags(WindowManager.LayoutParams.FLAG_BLUR_BEHIND);
//            dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
//                @Override
//                public void onDismiss(DialogInterface dialog) {
//
//                }
//            });
            dialog.setOnCancelListener(new DialogInterface.OnCancelListener() {
                @Override
                public void onCancel(DialogInterface dialog) {
                    RequestRadioActivity.this.finish();
                    onBackPressed();
                }
            });
            dialog.show();

        }
    }


}

